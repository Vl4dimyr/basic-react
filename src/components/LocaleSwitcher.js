import React from 'react'

import counterpart from 'counterpart'
import Translate from 'react-translate-component'

export default class LocaleSwitcher extends React.Component {
    constructor() {
        super()

        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(e) {
        counterpart.setLocale(e.target.value);
    }

    render() {
        const locales = counterpart.getAvailableLocales().map((locale, i) =>
            <Translate key={i} value={locale} content="language" locale={locale} component="option"/>)

        return (
            <div class="locale_switcher">
                <select defaultValue={counterpart.getLocale()} onChange={this.handleChange}>
                    {locales}
                </select>
            </div>
        );
    }
}
