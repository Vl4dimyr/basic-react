import React from "react"

import { Link } from "react-router"

import Translate from 'react-translate-component'

export default class Navigation extends React.Component {
    render() {
        return (
            <div>
                <ul>
                    <li><Link to="/"><Translate content="link.dashboard" /></Link></li>
                </ul>
            </div>
        )
    }
}
