import React from 'react'

import defaultStore from '../stores/DefaultStore'

import LocaleSwitcher from '../components/LocaleSwitcher'
import Navigation from '../components/Navigation'

export default class Base extends React.Component {
    render() {
        const { location, children } = this.props;

        const childrenWithDefaultStore = React.Children.map(children, child => React.cloneElement(child, {
            defaultStore
        }));

        return (
            <div>
                <div>
                    <LocaleSwitcher />
                    <Navigation location={location}/>
                </div>
                <div>
                    {childrenWithDefaultStore}
                </div>
            </div>
        )
    }
}
