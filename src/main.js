require('./styles/main.scss')

import React from "react"
import ReactDOM from "react-dom"

import { Router, Route, IndexRoute, hashHistory } from "react-router"

import BaseLayout from "./layouts/BaseLayout"

import HomePage from "./pages/HomePage"

import counterpart from 'counterpart'

const translations = require.context('./i18n/', true, /\.ya?ml/)

translations.keys().forEach(function (path) {
    const file = path.substr(path.lastIndexOf('/') + 1)
    const locale = file.split('.')[0]
    const lang = require('json!yaml!./i18n/' + file)

    const locales = counterpart.getAvailableLocales()

    counterpart.setLocale([
        ...locales,
        locale
    ])

    counterpart.registerTranslations(locale, lang)
});

counterpart.setFallbackLocale('en')
counterpart.setLocale((navigator.languages ? navigator.languages[0] : (navigator.language || navigator.userLanguage)).split('-')[0])

const app = document.getElementById('app')

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path="/" component={BaseLayout}>
            <IndexRoute component={HomePage} />
            {/*<Route path="" component={} />*/}
        </Route>
    </Router>,
    app
)
