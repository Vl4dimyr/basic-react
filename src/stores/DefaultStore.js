import { observable } from 'mobx'

class DefaultStore {
    @observable dummyData = 1337
}

const defaultStore = window.defaultStore = new DefaultStore

export default defaultStore
