import React from "react"

import { observer } from 'mobx-react'

import Translate from 'react-translate-component'

@observer
export default class HomePage extends React.Component {
    render() {
        return (
            <div>
                <h1><Translate content="heading.dashboard" /></h1>
                {this.props.defaultStore.dummyData}
            </div>
        )
    }
}
