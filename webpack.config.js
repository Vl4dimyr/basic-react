const debug = process.env.NODE_ENV !== 'production'
const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    context: path.join(__dirname, 'src'),
    devtool: debug ? 'inline-sourcemap' : null,
    entry: './main.js',
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: [
                        'react',
                        'es2015',
                        'stage-0'
                    ],
                    // IMPORTANT: transform-decorators-legacy before transform-class-properties otherwise (mobx) decorators won't work.
                    plugins: [
                        'react-html-attrs',
                        'transform-decorators-legacy',
                        'transform-class-properties'
                    ]
                }
            },
            {
                test: /\.ya?ml$/,
                include: path.resolve('data'),
                loader: 'yaml-loader'
            },
            {
                test: /main\.scss$/,
                loader: ExtractTextPlugin.extract('style', 'css!sass')
            }
        ]
    },
    output: {
        path: __dirname + '/web/',
        filename: 'main.min.js'
    },
    plugins: debug ? [
            new ExtractTextPlugin('main.min.css')
        ] : [
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.OccurenceOrderPlugin(),
            new webpack.optimize.UglifyJsPlugin({
                mangle: false,
                sourcemap: false
            }),
            new ExtractTextPlugin('main.min.css')
        ]
}
