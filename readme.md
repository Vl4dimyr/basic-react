# Basic React

This is a basic react project containing the following:

- SCSS
- ES6/JSX
- Translations ([react-translate-component](https://github.com/martinandert/react-translate-component))
- Stores ([mobx](https://github.com/mobxjs/mobx))

# Requirements (i used)

- npm 4.0.5 ([install](https://www.npmjs.com/package/npm))
- node 6.9.1 (i used [n to install](https://github.com/tj/n))

# Usage

Purpose    | Command
---        | ---
install    | `npm i`
dev        | `npm run dev`
production | `npm run webpack`

Running the **dev** command will start a [webpack-dev-server](https://webpack.github.io/docs/webpack-dev-server.html) at port 8080 (see package.json for details)

# License

[MIT](https://opensource.org/licenses/MIT)
